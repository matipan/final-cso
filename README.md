# Sistemas operativos
Un sistema operativo es un programa que controla la ejecucion de aplicaciones y programas y que actua como una interfaz entre las aplicaciones y el hardware del computador. Se considera que un sistema operativo tiene los siguientes objetivos:
### Facilidad de uso
El hardware y software utilizados por un computador se puede representar en capas:
![capas-so](https://gitlab.com/matipan/final-cso/raw/master/capas.png)  
El usuario final ve un sistema de computacion como un conjunto de aplicaciones realizadas por el programador con la ayuda de las utilidades que provee el sistema operativo.  
El SO logra ocultar los detalles del hardware al programador, actua como mediador proporcionandole una interfaz apropiada para utilizar el sistema y facilitandole la labor al programador. Suele proporcionar servicios en las siguientes areas:
* **Desarrollo de programas, ejecucion de programas, acceso a dispositivos de E/S, acceso controlado a ficheros, acceso al sistema, deteccion y respuesta a errores, contabilidad**.

### Eficiencia y gestion de recursos
Un computador es un conjunto de recursos que se utilizan para el transporte, almacenamiento y procesamiento de los datos, asi como para llevar a cabo el control de estas funciones. El sistema operativo se encarga de gestionar estos recursos.  
El SO es un **conjunto de programas** que proporciona instrucciones al procesador, estas instrucciones dirijen al procesador en el uso de otros recursos del sistema y en la temporizacion de la ejecucion de otros programas. Para que el procesador pueda llevar a cabo eso, el SO debe dejar paso a la ejecucion de otros programas, cediendo control para que pueda realizar trabajo util y retomando el control para que realice la siguiente pieza de trabajo.  

Como se puede ver en la siguiente figura, una porcion del SO se encuentra en memoria:
![gestor-recursos](https://gitlab.com/matipan/final-cso/raw/master/so-gestor-recursos.png)  
Esa porcion se denomina **Kernel** y contiene las funciones del sistema operativo mas frecuentemente utilizadas. El resto de la memoria se compone de datos y programas del usuario. La asignacion de la memoria principal es gestionada de forma conjunta por el sistema operativo y el hardware de gestion de memoria del procesador.  
Se puede observar que el SO es el que decide cuando un programa en ejecucion puede utilizar un dispositivo de E/S y controla el acceso y uso de los ficheros.

### Facilidad de evolucion
El SO debe evolucionar en el tiempo por las constantes actualizaciones del hardware, los nuevos requisitos de los usuarios y fallos que se encuentran en versiones anteriores.  
Esta necesidad de estar constantemente actualizando el SO introduce ciertos requisitos en el diseño del mismo.

## Evolucion del SO

### Procesamiento en serie
En un principio el programador interraccionaba directamente con el hardware del computador ya que no habia sistema operativo. Eran maquinas utilizadas desde una consola que contenia luces, interruptores, algun dispositivo de entrada y una impresora. A esta se le cargaban programas en codigo de maquina mediante el dispositivo de entrada. Si un error provocaba la parada del programa, las luces indicaban la condicion de error.  
Se puede observar que los usuarios accedian al computador en serie.

### Sistemas en lotes sencillos
Para mejorar la utilizacion de las primeras maquinas, las cuales eran muy caras y no se podia desperdiciar su uso, se desarrollo el concepto de sistema operativo en lotes.  
La idea bajo el esquema de procesamiento en lotes sencillo es usar un software denominado **monitor**, gracias al cual el usuario ya no tiene que acceder directamente a la maquina. Sino que envia un trabajo a traves de una tarjeta o cinta al operador del computador, que crea un sistema por lotes con todos los trabajos enviados y coloca la secuencia de trabajos en el dispositivo de entrada para que lo utilice el monitor.  

#### Punto de vista del monitor
Este controla la secuencia de eventos. Una gran parte del monitor debe estar en memoria principal y disponible para la ejecucion, denominada monitor residente. El resto esta formado por un conjunto de utilidades y funciones comunes que se cargan como subrutinas en el programa de usuario al comienzo de cualquier trabajo que las necesite. Este lee los trabajos desde el dispositivo de entrada, una vez leido, el trabajo se coloca en el area de programa de usuario y se le pasa el control, cuando termina se devuelve el control al monitor. Los resultados se devuelven por salida para entregarlos al usuario.  
![disp-monitor-residente](https://gitlab.com/matipan/final-cso/raw/master/disp-monitor-residente.png)

#### Punto de vista del procesador
En un cierto punto, el procesador ejecuta instrucciones de la zona de memoria principal que contiene el monitor. Estas instrucciones provocan que se lea el siguiente trabajo y se almacene en otra zona de memoria principal. Una vez que el trabajo se ha leído, el procesador encontrará una instrucción de salto en el monitor que le indica al procesador que continúe la ejecución al inicio del programa de usuario. El procesador entonces ejecutará las instrucciones del programa usuario hasta que encuentre una condición de finalización o de error.  
Cualquiera de estas condiciones hace que el procesador ejecute la siguiente instrucción del programa monitor.  


El monitor, o sistema operativo en lotes, es simplemente un programa, el cual confia en la habilidad del procesador para cargar instrucciones de diferentes porciones de la memoria principal que de forma alternativa le permiten tomar y abandonar el control. Otras caractiristicas hardware son deseables:
* **Proteccion de memoria**
* **Temporizador**
* **Instrucciones privilegiadas**
* **Interrupciones**

Ciertas consideraciones sobre la proteccion de memoria y las instrucciones privilegiadas llevan al concepto de modos de operacion. Un programa de usuario se ejecuta en **modo usuario**, donde estos no pueden acceder a ciertas areas de memoria y no pueden ejecutar ciertas operaciones. El monitor se ejecuta en modo sistema denominado **modo nucleo**, en el cual se pueden ejecutar instrucciones privilegiadas y se puede acceder a areas de memoria protegidas.  

Con un **sistema operativo en lotes**, el tiempo de maquina alterna la ejecucion de programas de usuario y la ejecucion del monitor, lo cual causa que el monitor utilice parte de la memoria y principal y consuma parte del tiempo de maquina.

### Sistemas en lotes multiprogramados
Con el sistema operativo en lotes el procesador se encuentra frecuentemente ocioso, debido a los dispositivos de E/S que son lentos comparados con el procesador.

Para evitar esta inficiencia se puede expandir la memoria para que entren 2 o mas programas de usuario y multiplexar el uso del procesador entre ellos. Por ejemplo, si un programa de usuario debe esperara a la salida de un dispositivo de E/S, otro programa que no necesite E/S toma el procesador y hace su trabajo.

Para la multiprogramacion, es importante que el hardware soporte las interrupciones de E/S y DMA. Con la E/S gestionada a traves de interrupciones o DMA, el procesador puede solicitar un mandato de E/S para un trabajo y continuar con la ejecucion de otro trabajo mientras el controlador del dispositivo gestiona la operacion de E/S.

Los sistemas operativos multiprogramados son bastante sofisiticados. Para tener varios trabajos listos para ejecutar, se deben guardar en memoria principal, lo que requiere alguna forma de **gestion de memoria**. Ademas, si varios trabajos estan listos para ejecutar, el procesador debe usar algun **algoritmo de planificacion** para decidir cual ejecutar.

### Sistemas de tiempo compartido
A veces es deseable proporcionar un modo en el cual el usuario interaccione directamente con el computador. Hoy en dia, los computadores personales cumplen los requisitos que necesitan una utilidad de computacion interactiva, pero en los años 60, cuando la mayoria de los computadores eran grandes y costosos, esto no era posible. Por eso se invento el concepto de tiempo compartido.

De la mismo forma que la multiprogramacion permite al procesador gestionar multiples trabajos en lotes en un determinado tiempo, tambien puede utilizarse para gestionar multiples trabajos interactivos. Esa tecnica se denomina tiempo compartido, porque se comparte el tiempo de procesador entre multiples usuarios.

En un sistema de tiempo compartido, multiples usuarios acceden simultaneamente al sistema a traves de terminales, siendo el sistema operativo el encargado de entrelazar la ejecucion de cada programa de usuario en pequeños intervalos de tiempo. Por eso, si hay n usuarios activos, cada usuario vera 1/n de la capacidad de computacion efectiva.

Ambos tipos de procesamiento, lotes y tiempo compartido, utilizan multiprogramacion, las principales diferencias son:
![multiprogramacion](https://gitlab.com/matipan/final-cso/raw/master/multiprogramacion.png)

La comparticion de tiempo y la multiprogramacion implican nuevos problemas para el sistema operativo. Si existen multiples trabajos en memoria, estos deben protegerse para evitar que interfieran entre si. Ademas, con multiples usuarios interactivos, el sistema de ficheros debe ser protegido, de manera que solo usuarios autorizados tengan acceso a un fichero en particular.

### System calls
Las llamadas al sistema se encuentran en la interfaz entre los programas de usuario y el sistema operativo, estas son las que tratan con las abstracciones de los recursos del computador.

La mayoria de los sistemas operatios modernos tienen llamadas al sistema que realizan las mismas funciones, pero difieren en los detalles.

Cualquier computadora con una sola CPU puede ejecutar solo una instruccion a la vez. Si un proceso esta ejecutando un programa de usuario en modo usuario y necesita un servicio del sistema, tiene que ejecutar una instruccion de trap para transferir el control al sistema operativo. Despues, el sistema operativo averigua que es lo que quiere el proceso llamador, para lo cual inspecciona los parametros. Luego lleva a cabo la llamada al sistema y devuelve el control a la instruccion que va despues de la llamada al sistema. En cierto sentido, realizar una llamada al sistema es como realizar un tipo especial de llamada a un procedimiento, solo que **las llamdas al sistema entran al kernel** y las llamadas a procedimientos no.

Los servicios ofrecidos por estas llamadas determinan la mayor parte de la labor del sistema operativo,. Estos servicios suelen ser acciones como la creacion y terminacion de procesos, la manipulacion de ficheros y las operaciones de E/S.

Inicialmente, el programa que esta realizando la llamada al sistema primero mete los parametros correspondientes a la llamada en la pila, estos parametros pueden ser pasados por valor o por referencia.  
Luego viene la llamada al procedimiento de biblioteca. Este procedimiento por lo general pone el numero de la llamada al sistema en un lugar en el que el sistema operativo lo espera, como por ejemplo un registro. Luego, procede a ejecutar una instruccion TRAP para generar un cambio del modo de ejecucion, es decir para pasar de modo usuario a modo kernel y empezar la ejecucion en una direccion fija dentro del nucleo.  
Ahora el codigo del kernel examina el numero de llamada al sistema mencionado previamente(el cual fue almacenado en un registro), y despues le pasa al manejador correspondiente de llamadas al sistema. Esto lo hace mediante una tabla de apuntadores a manejadores de llamadas al sistema. Es ahora cuando se ejecuta el manejador de llamadas al sistema, es decir se atiende la system call.  
Una vez que termina de atenderla, se regresa el control al procedimiento de biblioteca que esta en el espacio de usuario, en la instruccion siguiente a la que causo el TRAP, luego se vuelve al programa de usuario. Finalmente el programa del usuario debe limpiar la pila, como lo hace despues de cualquier llamada a un procedimiento.  
Cabe destacar que la llamada al sistema puede bloquear al procedimiento llamador, evitando que continue su ejecucion. En este caso, el SO va a buscar otro proceso para ejecutarlo mientras se atiende la llamada realizada.

Los 11 pasos que se realizan cuando se ejecuta una llamada `read(fd, buffer, bytes)` en un programa en C, son:
![syscall-read](https://gitlab.com/matipan/final-cso/raw/master/syscall-read.png)

Hay distintos tipos de llamadas que se realizan al sistema operativo, algunas de estas son:
* **Llamadas al sistema para la administracion de procesos**, como por ejemplo `fork`  
  fork es la unica forma de crear un nuevo proceso en POSIX. Esta crea un duplicado exacto del proceso original, incluyendo todos los descriptores de archivos, registros y demas. Despues del fork, los dos procesos se van por caminos separados. Al momento de la llamada del fork, las variables de los distintos procesos tienen el mismo valor, pero luego pueden ser modificados y NO van a ser reflejados.  
  Normalmente luego de un fork el proceso padre se va a quedar esperando a que el hijo termine su ejecucion. Para esto utiliza una llamada al sistema denominada `waitpid`, la cual espera a que cualquier hijo termine. Luego de ejecutar el fork, el proceso hijo hace una llamada al sistema denominada `exec`
* **Llamadas al sistema para la administracion de archivos directorios**, como por ejemplo `mkdir` o `link`  
  Para leer o escribir un archivo, primero debe abrirse mediante la llamada `open`. Esta especifica el nombre del archivo que se va a abrir, y un codigo que indica el modo de apertura. (O_CREAT para crear un nuevo archivo).
* **Llamadas varias** como por ejemplo `chdir`, `chmod` o `kill`


# Procesos
Se le pueden dar varias definiciones al termino de **proceso**. Algunas de estas son:
* Un programa en ejecucion
* Una instancia de un programa ejecutando en un computador
* Una unidad de actividad que se caracteriza por la ejecucion de una secuencia de instrucciones, un estado actual y un conjunto de recursos del sistema asociados.

### Process control block
Se lo puede pensar como una entidad que consiste de un numero de elementos. Los dos elementos esenciales serian el **codigo de programa** y un **conjunto de datos** asociados a dicho codigo. Denominamos proceso a la entidad resultante de el procesador ejecutando el codigo de programa mencionado. Mientras el proceso esta en ejecucion, se lo puede caracterizar por un conjunto de elementos:
* **Identificador**, un ID unico asociado a este proceso. Denominado PID
* **Estado** actual del proceso(en ejecucion, frenado, etc)
* **Prioridad** nivel de prioridad relativa al resto de procesos
* **Contador de programa** que indica la direccion de la siguiente instruccion a ejecutar.
* **Punteros a memoria**, los puntos al codigo de programa y los datos asociados, ademas de cualquier otro bloque de memoria compartido con otro proceso.
* **Datos de contexto**, datos que estan presente en los registro del procesador mientras el proceso esta corriendo
* **Informacion de estado de E/S**, donde estan las peticiones de E/S pendientes, dispositivos de E/S asignados a dicho proceso y mas.
* **Informacion de auditoria**, cantidad de tiempo de procesador y tiempo de reloj utilizados, asi como limites de tiempo y mas.

La informacion de esa lista se almacena en una estructura de datos, la cual se denomina PCB: bloque de control de proceso, esta es creada y gestionada por el sistema operativo. Lo mas importante de esta estructura, es que contiene la informacion necesaria para poder interrumpir el proceso y luego restaurar su estado de ejecucion como si nunca hubiese sido interrumpido. El PCB es la herramienta clave que permite al SO dar soporte a multiples procesos y proporcionar multiple programacion.  
En la siguiente tabla se puede ver en detalle la informacion que el SO necesita para cada proceso:
![pcb_1](https://gitlab.com/matipan/final-cso/raw/master/pcb_1.png)
![pcb_2](https://gitlab.com/matipan/final-cso/raw/master/pcb_2.png)

Para gestionar de manera correcta los procesos y recursos, el SO dispone de cuatro diferentes tables las cuales mantiene:
* **Tablas de memoria**: usadas para mantener un registro tanto de la memoria principal como de la secundaria
* **Tablas de E/S** para gestionar los dispositivos de E/S y los canales del computador.
* **Tablas de ficheros** las cuales proporcionan informacion sobre la existencia de ficheros, su posicion en almacenamiento secundario, su estado actual y otros atributos.
* **Tablas de procesos** para gestionar los procesos

**Cuando un proceso se interrumpe, los valores actuales del contador de programa y los registros del procesador se guardan en los campos correspondientes del BCP y el estado de proceso se cambia a cualquier otro valor, como bloqueado o listo. El SO es libre para poner otro proceso en estado de ejecucion. El PC y los datos de contexto se recuperan y cargan en los registros del procesador y este proceso comienza a correr.**

El **dispatcher(activador)** es un pequeño programa encargado de intercambiar el procesador de un proceso a otro, este lo hace mediante alarmas de temporizacion.

### Modelo de proceso de dos estados
Es un modelo muy simple donde se observa en un instante dado si un proceso esta siendo ejecutado por el procesador o no. En este modelo, un proceso puede estar en dos estados: **Ejecutando** o **No Ejecutando**:  
![procesos_estados_simples](https://gitlab.com/matipan/final-cso/raw/master/procesos_estados_simples.png)

Cuando el SO crea un nuevo proceso, crea el BCP para el nuevo proceso en inserta dicho proceso en el sistema en estado No Ejecutando. Cuando el activador interrumpa al procesador el SO elige el siguiente proceso a ejecutar, el cual pasara de No Ejecutando a Ejecutando.  
Los procesos que no estan ejecutando deben estar en una cola, esperando su turno de ejecucion:  
![procesos_cola_ejecucion](https://gitlab.com/matipan/final-cso/raw/master/procesos_cola_ejecucion.png)

#### Creacion de un proceso
Cuando se va a añadir un nuevo proceso a aquellos que se estan gestionando en un determinado momento, el SO construye las estructuras de datos que se usan para manejar el prceso y reserva el espacio de direcciones en memoria principal para el proceso.  
En un principio, el SO era el encargado de la creacion de todos los procesos de una forma que era transparante para el usario y los programas. Sin embargo, se vio que era muy util permitir a un proceso la creacion de otro, donde el nuevo proceso se ejecuta "en paralelo" con el proceso original y se activa cuando los nuevos datos estan disponibles. Cuando un proceso lanza a otro, al primero se lo denomina **proceso padre** y al proceso creado se lo denomina **proceso hijo**.

En detalle, la creacion de un proceso lleva los siguientes pasos:
  1. **Asignar un identificador de proceso unico al proceso**: se añade una nueva entrada la tabla primaria de procesos, que contiene una entrada por proceso.
  2. **Reservar espacio para proceso**: el SO debe conocer cuanta memoria se requiere para el espacio de direcciones privado(programas y datos) y para la pila de usuario.
  3. **Inicializacio del bloque de control de proceson**: La parte de identificacion de proceso del BCP contiene el identificador del proceso asi como otros identificadores, el PPID(parent).
  4. **Establecer los enlaces apropiados**
  5. **Creacion o expansion de otras estructuras de datos**

#### Terminacion de procesos
Todo sistema debe proporcionar los mecanismos mediante los cuales un proceso indica su finalizacion, o que ha completado su tarea. Por ejemplo, un trabajo por lotes debe incluir una instruccion HALT o una llamada a un servicio de SO especifica para su terminacion. En el caso de una aplicacion interactiva, las acciones del usuario indicaran la finalizacion del proceso. Adicionalmente, un numero de error o una condicion de fallo puede llevar a la finalizacion de un proceso.

### Modelo de proceso de cinco estados
Para hacer un mejor uso del procesador, y que un proceso que esta listo para ejecutar no pueda hacerlo debido a que otro esta bloqueado esperando una operacion de E/S, se divide el estado **No Ejecutando** en dos estados, **Listo** y **Bloqueado**. Para poder gestionarlo de manera adecuada, se agregan dos estados mas:
* **Ejecutando**, esta actualmente en ejecucion.
* **Listo**, proceso que esta listo para su ejecucion.
* **Bloqueado**, un proceso que no puede ejecutar hasta que se cumpla un evento determinado.
* **Nuevo**, procesos que se acaba de crear y que aun no ha sido admitido en el grupo de procesos ejecutables por el sistema operativo.
* **Saliente**, proceso que ha sido liberado del grupo de procesos ejecutables por el sistema operativo, debido que fue detenido o abortado.

La transicion de estos nuevos estados es:  
![procesos_estados_cinco](https://gitlab.com/matipan/final-cso/raw/master/procesos_estados_cinco.png)  

Las transiciones de los estados:
* **Null → Nuevo**: se crea un proceso para ejecutar un programa.
* **Nuevo → Listo**:El SO mueve a un proceso cuando este se encuentre preparado para ejecutar un nuevo proceso.
* **Listo → Ejecutando**: cuando llega el momento de seleccionar un nuevo proceso para ejecutar, el SO selecciona uno de los procesos que se encuentren en el estado Listo.
* **Ejecutando → Saliente**: el proceso actual en ejecucion se finaliza por parte del SO si el proceso indica que termino o si se aborta.
* **Ejecutando → Listo**: la razon mas habitual de esta transicion es que el proceso en ejecucion haya alcanzado el maximo tiempo posible de ejecucion de forma ininterrumpida. Otra causa puede ser cuando un SO asigna diferentes niveles de prioridad a diferentes procesos
* **Ejecutando → Bloqueado**: un proceso se pone en estado bloqueado si solicita algo por lo cual debe esperar. Una solicitud al SO se realiza por medio de una llamada al sistema(syscall). Por ejemplo, un proceso en ejecucion ha solicitado un servicio que el SO no puede realizar en ese momento.
* **Bloqueado → Listo**: un proceso en estado Bloqueado se mueve al Listo cuando sucede el evento por el cual estaba esperando.

### Procesos suspendidos
La razon principal por la que se crearon todos estados es por que las operaciones de E/S son mucho mas lentas que los procesos de computo, y por lo tanto, en un como sistema monoprogramado como en un sistema multiprograma el procesador puede estar ocioso la mayor parte del tiempo.  
Para solucionar esto se plantea el *swapping*, que implica mover parte o todo el proceso de memoria principal al disco. Cuando ninguno de los procesos en memoria principal se encuentra en estado **Listo**, el sistema operativo intercambia uno de los procesos bloqueados a disco, en la cola de **Suspendidos**. Esta es una lista de procesos existentes que han sido temporalmente expulsados de la memoria principal, o suspendidos. El SO trae otro proceso de la cola de **Suspendidos** o responde a una solicitud de un nuevo proceso.

Hay que tener en cuenta que el *swapping* es una operacion de E/S, por ende existe el riesgo de hacer que el problema empeore. De todas maneras, dado que la E/S del disco es por lo general mas rapida que el resto de las E/S, esta tecnica suele mejorar el rendimiento del sistema.

Con el uso del *swapping*, vemos que se agregar un nuevo estado, el estado **Suspendido**. Cuando todos los procesos en memoria principal se encuentran en estado **Bloqueado**, el SO puede suspender un proceso poniendolo en el estado **Suspendido** y transfiriendolo a disco, esto libera espacio en memoria principal que queda para traer un nuevo proceso. Este "nuevo proceso", puede que sea un proceso recientemente creado, o puede traer un proceso que estaba **Suspendido**.

Entonces, el nuevo diagrama de transiciones de estados es:
![procesos_estados_suspendido](https://gitlab.com/matipan/final-cso/raw/master/procesos_estados_suspendido.png)  
Las transiciones mas importantes a notar son:
* **Bloqueado → Bloqueado/Suspendido**: si no hay procesos listos, entonces al menos uno de los procesos bloqueados se transfiere al disco para hacer espacio para otro proceso que no se encuentra bloqueado.
* **Bloqueado/Suspendido → Listo/Suspendido**: el proceso se mueve cuando sucede un evento al que estaba esperando.
* **Listo/Suspendido → Listo**: Cuando no hay mas procesos listos en memoria principal, el SO necesitara traer uno para continuar la ejecucion.
* **Listo → Listo/Suspendido**: el SO suele preferir suspender procesos bloqueados antes que un proceso Listo, porque un proceso Listo se puede ejecutar en ese momento, mientras que un proceso Bloqueado ocupa espacio de memoria y no se puede ejecutar. Sin embargo, puede ser necesario suspender un proceso Listo con el fin de liberar un bloque grande de memoria.

### Modos de ejecucion
La mayoria de los procesadores proporcionan al menos dos modos de ejecucion ya que hay ciertas instrucciones que se pueden y deben ejecutar en un modo privilegiado.

El modo menos privilegiado se denomina **modo usuario**, ya que los programas de usuario son los que se suelen ejecutan en este modo. El modo mas privilegiado se denomina **modo sistema**, **modo control** o **modo nucleo**.

La razon principal por la que usan los otros modos es para proteger al sistema operativo y tablas clave del sistema, de la interferencia con programas de usuario. En **modo nucleo** el software tiene control completo del procesador y de sus instrucciones, registros y memoria. Este nivel de control no es necesario y por seguridad no es recomendable para los programas de usuario.

# Micronucleos
Un micronucleo es la pequeña parte central de un sistema operativo que proporciona las bases para extensiones modulares.

Los primeros sistemas operativos que fueron diseñados se denominaban **sistemas operativos monoliticos**, donde cualquier procedimiento podia llamar a cualquier otro. A medida que los sistemas operativos crecian, esta falta de estructura se hizo insostenible. Debido a esto, se decidio ir por una estructura modular dando lugar a los **sistemas operativos por capas**, en los cuales las funciones se organizan jerarquicamente y solo hay interaccion entre las capas adyacentes. Pero incluso con esta estructura habian problemas, cada capa contenia demasiada funcionanlidad, y eran muy dependientes una capa de otra, ya que si se hacia un cambio significante a una capa, era casi imposible saber como eso iba a afectar a las capas que la rodeaban.

Los micronucleos intentan minimizar el tamaño del nucleo, de manera que solo las funciones esenciales del sistema operativo esten en el nucleo. Los servicios y aplicaciones menos esenciales se construyen sobre el micronucleo y se ejecutan en modo usuario.

Como se puede ver en la siguiente figura la arquitectura del micronucleo reeemplaza la tradicional estructura vertical por una mas horizontal:
![micronucleo_capas](https://gitlab.com/matipan/final-cso/raw/master/micronucleo_capas.png)

Los componentes del sistema operativo externos al micronucleo se implementan como servidores de procesos; interactuan entre ellos dos a dos, normalmente por paso de mensajes a traves del micronucleo. Este funciona como como un intercambiador de mensajes: valida mensajes, los pasa entre los componentes, previene que el sea un intercambio no permitido y concede el acceso al hardware.

La organizacion micronucleo provee una serie de **ventajas**:
* **Interfaces uniformes**: este impone una interfaz uniforme en las peticiones realizadas por un proceso, por ende, los procesos no necesitan diferenciar entre servicios a nivel de nucleo y a nivel de usuario.
* **Extensibilidad**: facilita y permite agregar nuevos servicios. Ademas, cunado se añade una nueva caracteristica, solo el servidor relacionado necesita modificarse o añadirse.
* **Flexibilidad**: Ademas de poder añadir nuevas caracteristicas, se peuden eliminar las caracteristicas existentes para realizar una implementecion mas pequeña y mas eficiente.
* **Portabilidad** En esta arquitectura, todo o gran parte del codigo especifico del procesador esta en el micronucleo, por ende, los cambios necesarios para transferir el sistema a un nuevo procesador son menores y tienden a estar unidos en grupos logicos.
* **Fiabilidad**, un micronucleo pequeño se puede verificar de forma rigurosa y que solo utilice un numero pequeño de interfaces de programacion de aplicaciones hace mas sencillo producir un codigo de calidad para los servicios del SO que estan fuera del nucleo.
* Este lleva por si mismo **soporte de sistemas distribuidos**, ya que la comunicacion es mediante el envio de mensajes.
* Una arquitectura micronucleo funciona bien en el contexto de un **sistema operativo orientado a objetos**

### Rendimiento del micronucleo
Una desventaja que se suele mencionar del micronucleo es el rendimiento. Lleva mas tiempo construir y enviar un mensaje a traves del micronucleo, y aceptar y decodificar la respuesta, que hacer una simple llamada a un servicio.

Los micronucleos de primera generacion mostraban una perdida sustancial del rendimiento que continuaban a pesar de las optimizaciones que se le hicieron al codigo. Para resolver esto se hicieron un poco mayores los micronucleos, volviendo a intrudocur servicios criticos y manejadores en el sistema operativo. Esta solucion reducia los problemas de rendimiento pero para esto sacrificaba la fortaleza del diseño del micronucleo.

Otro enfoque para resolver el problema del rendimiento era hacerlo mas pequeño. Para dar una idea de que tan pequeño, el tipico micronucleo de primera generacion tenia 300Kbytes de codigo y 140 interfaces de llamadas al sistema. Un ejemplo de micronucle de segunda generacion puede consistir en 12Kbytes de codigo y 7 llamadas al sistema.

### Diseño del micronucleo
El micronucleo debe incluir aquellas funciones que dependen directamente del hardware y aquellas funciones necesarias para mantener a los servidores y aplicaciones operando en modo usuario.

#### Gestion de memoria a bajo nivel
El micronucleo se debe responsabilizar de la asignacion de cada pagina virtual a un marco fisico, luego la parte principal de gestion de memoria, incluyendo proteccion del espacio de memoria entre procesos, el agoritmo de reemplazo de paginas y otra logica de paginacion, puede implementarse fuera del nucleo.

Esto permite a un proceso fuera del nucleo proyectar archivos y bases de datos en espacios de direcciones de usuario sin llamar al nucleo. Las politicas de comparticion de memoria especificas de la aplicacion se pueden implementar fuera del nucleo.

#### Comunicacion entre procesos
La forma basica de comunicacion entre dos procesos en un SO con micronucleo son los **mensajes**. Un mensaje incluye una cabecera que identifica a los procesos remitente y receptor y un cuerpo que contiene los datos, un puntero a un bloque de datos o alguna informacion de control del proceso. Podemos pensar que las IPC(interprocess communication) se fundamentan en puertos asociados a cada proceso, donde un **puerto** es una cola de mensajes destinada a un proceso particular y un proceso puede tener multiples puertos. Asociado a cada puerto existe una lista que indica que procesos se pueden comunicar con este.

#### Gestion de E/S e interrupciones
Con una arquitectura micronucleo es posible manejar las interrupciones hardware como mensajes e incluir los puertos de E/S en los espacios de direcciones. El micronucleo puede reconocer las interrupciones que esta pero no las puede manejar. Mas bien, genera un mensaje para el proceso a nivel de usuario que esta actualmente asociado con esa interrupcion. Las tranformaciones de interrupciones en mensajes las debe realizar el micronucleo, pero este no esta relacionado con el manejo de interrupciones especifico de los dispositivos.

# Memoria
Los requisitos que la gestion de la memoria debe satisfacer son:
* **Reubicacion**
* **Proteccion**
* **Comparticion**
* **Organizacion logica**
* **Organizacion fisica**

### Reubicacion
En un sistema multiprogramado, la memoria principal disponible se comparte generalmente entre varios procesos. Seria bueno poder intercambiar procesos en la memoria principal para maximizar la utilizacion del procesador, proporcionando un gran numero de procesos para la ejecucion. Una vez que un proceso se ha llevado al disco, seria bstante limitante tener que colocarlo en la misma region de memoria principal donde estaba. Entonces, podria ser necesario **reubicar** el proceso a un area de memoria diferente. Por lo tanto, no se puede conocer de forma anticipada donde se va a colocar un programa y se debe permitir que los programas se puedan mover en la memoria principal.

### Proteccion
Cada proceso debe protegerse contra interferencias no deseadas por parte de otros procesos, sean accidentales o intencionadas. Por ende, programas de otros procesos no deben ser capaces de referenciar sin permiso posiciones de memoria de un proceso, tanto en modo lectura como escritura. Para esto, todas las referencias de memoria generadas por un proceso deben comprobarse en tiempo de ejecucion para poder asegurar que se refieren solo al espacio de memoria asignado a dicho proces

Los requisitos de proteccion de memoria deben ser satisfechos por el procesador en lugar del sistema operativo. Esto es debido a que el sistema operativo no puede anticipar todas las referencias de memoria que un programa hara. Incluso si fuese posible, tardaria demasiado tiempo calcularlo para cada programa. Por lo tanto, solo es posible evaluar la permisibildad de una referencia en tiempo de ejecucion de la instruccion que realiza dicha referencia.

### Comparticion
Cualquier mecanismo de proteccion debe tener la flexibilidad de permitir a varios procesos acceder a la misma porcion de memoria principal. Por ende, el sistema de gestion de la memoria debe permitir el acceso controlado a areas de memoria compartidas sin comprometer la proteccion esencial.

### Organizacion logica
La mayoria de los programas se organizan en modulos, algunos de los cuales no se pueden modificar, y algunos contienen datos que se pueden modificar. Si el SO y el hardware pueden tratar de forma efectiva los programas de usuarios y los datos en la forma de modulos de algun tipo, entonces:
* Los modulos se pueden escribir y compilar independientemente
* Se puede proporcionar diferentes grados de proteccion a los modulos.
* Se puede introducir mecanismos por los cuales los modulos se pueden compartir entre procesos.

### Organizacion fisica
La memoria del computador se organiza en *al menos* dos niveles, memoria principal y memoria secundaria. La memoria principal es rapida y volatil, la memoria secundaria es mas lenta, barata y no es volatil. Por lo tanto, la memoria secundaria de larga capacidad puede proporcionar almacenamiento para programas y datos a largo plazo, mientras que la memoria principal contiene programas y datos actualmente en uso.

La organizacion del flujo de informacion entre la memoria princiapl y la secundaria es crucial, y no se le puede asignar la tarea al programador ya que la memoria principal para un programa mas sus datos puede ser insuficiente, en tal el programador deberia utilizar alguna tecnica de superposicion, lo cual no es conveniente y malgasta el tiempo del programador. Ademas, en un entorno multiprogramado, el programador no conoce en tiempo de codificacion cuanto espacio estara disponible o donde se localizara dicho espacio.

Por ende, la tarea de mover la informacion entre los dos niveles de la memoria deberia ser una responsabilidad del sistema.

## Asignacion de memoria
La operacion principal de la gestion de la memoria es traer los procesos a la memoria principal para que el procesador los pueda ejecutar. En casi todos los sistemas multiprogramados modernos esto implica el uso de un esquema sofisticado denominado memoria virtual.

Una tecnica mas sencilla que no utiliza memoria virtual se denomina **particionamiento**.

### Particionamiento fijo
El sistema operativo ocupa una porcion fija de la memoria principal, el resto de esta esta disponible para multiples procesos, el cual se reparte en regiones con limites fijos.

Una posibilidad consiste en hacer uso de particiones del mismo tamaño, de esa manera si un proceso cuyo tamaño es menor o igual que el tamaño de particion puede cargarse en cualquier particion disponible. De todas formas, existen dos dificultades con este esquema:
* Un programa podria ser demasiado grande para caber en una particion. El programador debe diseñar el programa con el uso de overlays, de forma que solo se necesite una porcion del programa en memoria principal en un momento determinado.
* La utilizacion de la memoria principal es extremadamente ineficiente. Cualquier programa, sin importa que tan pequeño sea, ocupa una particion entera.

#### Algoritmo de ubicacion
**Con particiones del mismo tamaño**, la ubicacion de los procesos en memoria es trivial. Cuando haya una particion disponible, un proceso se carga en esa particion, la desventaja que posee es que si un proceso ocupa nada mas un porcentaje de la particion, el resto queda sin ser usado, por ende se desperdicia memoria principal, esto se denomina **fragmentacion interna**. Si todas las particiones se encuentran ocupadas por procesos que no estan listos para ejecutar, entonces uno de dichos procesos debe llevarse a disco para dejar espacio para un nuevo proceso.

Con **particiones de diferente tamaño** hay dos formas de asignar los procesos a las particiones. La forma mas simple consiste en asignar cada proceso a la particion mas pequeña dentro de la cual cabe. Para esto se necesita una cola de planificacion para cada particion, que mantiene procesos en disco destinados a esa particion. La ventaja de esta tecnica es que se minimiza la memoria malgastada dentro de una particion.

Una tecnica mas optima seria emplear una cola unica para todos los procesos. En el momento de cargar un proceso en la memoria principal, se selecciona la particion mas pequenna disponible que pueda albergar dicho proceso.

El uso de particiones de distinto tamaño proporciona un grado de flexibilidad frente a las particiones fijas. Sin embargo, sus desventajas son:
* El numero de particiones especificadas en tiempo de generacion del sistema limita el numero de procesos activos del sistema.
* Dado que los tamaños de las particiones son preestablecidos en tiempo de generacion del sistema, los trabajos pequeños no utilizan el espacio de las particiones eficientemente.

### Particionamiento dinamico
Las particiones son de longitud y numero variable. Cuando se lleva un proceso a la memoria principal, se le asigna exactamente tanta memoria como requiera y no mas. La siguiente figura ilustra como funcionaria:
![particionamiento_dinamico](https://gitlab.com/matipan/final-cso/raw/master/particionamiento_dinamico.png)

Como ese ejemplo muestra, el metodo comienza correctamente, pero finalmente lleva a una situacion en la cual existen muchos huecos pequeños en la memoria. Esto se denomina **fragmentacion externa**.

Una tecnica que se usa para eliminar la fragmentacion externa es la **compactacion**, donde el sistema operativo, de vez en cuando, desplaza los procesos para que estos se encuentren contiguos y que la memoria libre se encuentre unida en un bloque. La desventaja de esta tecnica es que consume tiempo y malgasta tiemp ode procesador.

#### Algoritmo de ubicacion
A la hora de cargar o intercambiar un proceso al a memoria principal, y siempre que haya mas de un bloque de memoria libre de suficiente tamaño, el sistema operativo debe decidir que bloque libre asignar. Para esto existen tres algoritmos de colocacion. Estos son:
* **mejor-ajuste**: donde escoge el bloque mas cercano en tamaño a la peticion.
* **primer-ajuste**: comienza a analizar la memoria desde el principio y escoge el primer bloque disponible que sea suficientemente grande.
* **siguiente-ajuste**: comienza a analizar la memoria desde la ultima colocacion y elige el siguiente bloque disponible que sea suficientemente grande.

El algoritmo **primer-ajuste** es el mas sencillo y el que normalmente trabaja mas rapido. El de **siguiente-ajuste** tiende a producir resultados ligeramente peores que el primer-ajuste, lleva frecuentemente a una asignacio de un bloque libre al final de la memoria, por lo que se puede requerir mas frecuentemente la compactacion. El algoritmo **mejor-ajuste** suele ser el peor, ya que este busca el bloque mas pequeño que satisfaga la peticion, garantiza que el fragmento que queda sea lo mas pequeño posible.

## Paginacion
Para resolver la **fragmentacion externa e interna**, supongase que la memoria principal se divide en porciones de tamaño fijo relativamente pequeños, y que cada proceso tambien se divide en porciones pequeñas del mimso tamaño fijo. A dichas porciones del proces, conocidas como **paginas**, se les asgina porciones disponibles de memoria, conocidas como **marcos**.

La siguiente figura demuestra el uso de paginas y marcos:
![paginas_marcos](https://gitlab.com/matipan/final-cso/raw/master/paginas_marcos.png)

El sistema operativo mantiene una lista de todos los marcos libres. En un momento dado, se quiere cargar un proceso A que necesita 4 paginas, para eso, el sistema operativo encuentra 4 marcos libres y carga el las paginas en esos cuatro marcos. Luego, se cargan el proceso B y el C que tienen 3 y 4 paginas correspondientemente. En un momento dado, el proceso B se encuentra suspendido, por lo que se hace un intercambio y se lo lleva al disco. Luego, el proceso D compuesto de 5 paginas se carga en la memoria.

Como se puede observar en el ejemplo anterior, que no hayan suficientes marcos libres contiguos no le importa al sistema operativo. Ya que este mantiene una tabla de paginas por cada proceso, esta tabla muestra la ubicacion del marco por cada pagina del proceso. Cada entrada de la tabla de pagians contiene el numero del marco en la memoria principal, si existe, que contiene la pagina correspondiente.

Dentro de cada programa, cada direccion logica esta formada por un numero de pagina y un desplazamiento dentro de la pagina. Con paginacion, la traduccion de direcciones logicas a fisicas las realiza tambien el hardware del procesador. Presentado como una direccion logica, el procesador utiliza la tabla de paginas para producir una direccion fisica.

En la paginacion el sistema operativo debe realizar trabajo de paginacion cuando se crea un proceso, se ejecuta un proceso, ocurre un fallo de pagina y se termina un proceso.  
Cuando se crea un proceso, el SO debe determinar que tan grandes seran el programa y los datos al principio, y crear una tabla de paginas para ellos. Se debe asignar el espacio en memoria para la tabla de paginas y se tiene que inicializar. Esta tabla debe estar en memoria cuando el proceso se esta ejecutando.  
Cuando ocurre un fallo de pagina, el SO tiene que leer los registros de hardware para determinar cual direccion virtual produjo el fallo. Con esa informacion debe calcular que pagina se necesita y localizarla en el disco. Despues debe buscar un marco de pagina disponible para colocar la nueva pagina, desalojando alguna pagina anterior si es necesario. Luego debe leer la pagina necesaria y colocarla en el marco de paginas.  
Cuando un proceso termina, el SO debe liberar su tabla de paginas, sus paginas y el espacio en disco que ocupan las paginas cuando estan en disco. Si alguna de las paginas estan compartidas con otros procesos, las paginas en memoria y en disco solo pueden liberarse cuando el ultimo proceso que las utilice haya terminado.

## Segmentacion
Un programa de usuario se puede subdividir utilizando segmentacion, en la cual el programa y sus datos asociados se dividen en un numero de **segmentos**. Hay una longitud maxima de segmento. Como en el caso de la paginacion, una direccion logica utilizando segmentacion esta compuesta de dos partes, un numero de segmento y un desplazamiento.

Debido al uso de segmentos de distinto tamaño, la segmentacion es similar al particionamiento dinamico. La diferencia, es que un programa podria ocupasr mas de una particion, y estas particiones no necesitan estar contiguas. La segmentacion **elimina la fragmentacion interna** pero sufre de fragmentacion externa. Sin embargo, dado que el proceso se divide en varias piezas mas pequeñas, la fragmentacion externa deberia ser menor.

La segmentacion, a diferencia de la paginacion, es visible al programador y se proporciona como una utilidad para organizar programas y datos. El inconveniente principal de este servicio es que el programador debe ser consciente de la limitacion de tamaño de segmento maximo.

Otra consecuencia de utilizar segmentos de distintos tamaños es que no hay una relacion simple entre direcciones logicas y direcciones fisica. Un esquema de segmentacion sencillo haria uso de una tabla de segmentos por cada proceso y una lista de bloques libres de memoria principal. Cada entrada de la tabla de segmentos tendria que proporcionar la direccion inicial de la memoria principal del correspondiente segmento. La entrada tambien deberia proporcionar la longitud del segmento, para asegurar que no se utilizan direcciones no validas.

En la segmentacion simple, un proceso se divide en un conjunto de segmentos que no tienen que ser del mismo tamaño. Cuando un proceso se trae a memoria, todos sus segmentos se cargan en regiones de memoria disponibles, y se crea la tabla de segmentos.

## Memoria dinamica
Se vio que con paginacion el espacio de direcciones de un proceso no tenia la necesitad de estar contiguo en la memoria para poder ejecutarse. Entonces, podemos pensar que no todo el espacio de direcciones del proceso se necesita en todo momento. De esta manera, el sistema operativo podria traer a memoria las piezas de un proceso a medida que este las va necesitando.

Inicialmente el so trae la porcion inicial del programa y la procion inicial de datos sobre la cual acceden las primeras instrucciones. Esta parte del proceso que se encuentra en memoria en cualquier instante de tiempo dado se denomina **conjunto residente** del proceso. Cuando el proceso esta ejecutandose, las cosas ocurren de forma sueva mientras que todas las referencias a la memoria se encuentren dentro del conjunto residente. Si el procesador encuentra una direccion logica que no se encuentra en la memoria principal, generara una interrupcion indicando un fallo de acceso a la memoria. El SO coloca al proceso interrumpido en un estado de bloqueado y toma el control. Para que la ejecucion se pueda reanudar, el so necesita traer a memoria principal la porcion del proceso que contiene la direccion logica ha causado el fallo de acceso.

Esto permite que:
* **Puedan mantenerse un mayor numero de proceso en memoria principal**, dado que solo se va a cargar algunas de las porciones de los procesos a ejecutar.
* **Un proceso pueda ser mayor que toda la memoria principal**, el programador ya no se debe preocupar por cuanta memoria esta disponible, ya que el trabajo de "fragmentar" el programa lo realiza el sistema operativo y el hardware.

La memoria virtual entonces permite una multiprogramacion muy efectiva que libera al usuario de las restricciones excesivamente fuertes de la memoria principal.

Para esto, es necesario que el hardware soporte paginacion por demanda y/o segmentacion. Se necesita tambien un dispositivo de memoria secundaria que de el apoyo para almacenar las secciones del proceso que no estan en memoria principal. Y el SO debe ser capaz de manejar el movimiento de las paginas o segmentos entre la memoria principal y la secundaria.

### Memoria virtual y paginacion
Para la memoria virtual basada en el esquema de paginacion se necesita una tabl de paginas, donde se asocia una unica table de paginas a cada proceso, pero en este caso, las entradas de la tabla son mas complejas. Se necesita que cada entrada indica si la correspondiente pagina esta presenta en memoria princiapl o no, mediante el bit P(presente). Se necesita otro bit M, de modificado, que indica si los contenidos de la pagina han sido alterados, si lo fueron deben ser escritos reflejados en memoria secundaria.

#### Fallo de pagina
Estos ocurren cuando el proceso intenta usar una direccion que esta en una pagina que no se encuentra en la memoria principal, es decir, Bit P=0. Cuando el hardware detecta el fallo, genera un trap al S.O. Este podra colocar el proceso en estado de blocked mientras gestiona que la pagina que se necesite se cargue. El SO busca un marco libre en la memoria y genera una operacion de E/S al disco para copiar en dicho marco la pagina del proceso que se necesita utilizar, mientras que se completa esta operacion, se puede asignar otro proceso al procesador. Cuando esta operacion finaliza, se notifica al SO el cual actualiza la tabla de paginas del proceso, retorna el proceso que genero el fallo a estado de Listo y cuando este se ejecuta, se volvera a ejecutar la instruccion que genero el fallo de pagina.

#### Tablas de pagina
En la mayoria de sistemas, existe una unica tabla de pagina por proceso. Pero cada proceso puede ocupar una gran cantidad de memoria virtual. Lo que resulta que la cantidad de memoria demandada por las tablas de pagina unicamente puede ser inaceptablemente grande. Para resolver este problema, la mayoria de esquemas de memoria virtual almacenan las tablas de pagina en memoria virtual, en vezde real. Esto quiere decir que las tablas de paginas estan sujetas a paginacion igual que cualquier otra pagina. Cuando un proceso esta en ejecucion, al menos parte de su tabla de paginas debe encontrarse en memoria, incluyendo la entrada de tabla de paginas de la pagina actualmente en ejecucion.

Algunos procesadores utilizan un esquema de **dos niveles** para organizar las tablas de paginas de gran tamaño. En este, existe un directorio de paginas, en el cual cada entrada apuntaba a una tabla de paginas. Entonces, si la extension del directorio de pagians es X, y la longitud maxima de una tabla de paginas es Y, entonces un proceso consistira en hasta X >= Y paginas.

 Una tipica entrada en la tabla de paginas seria asi:
![paginas_tabla_entrada](https://gitlab.com/matipan/final-cso/raw/master/paginas_tabla_entrada.png)
El campo mas importante es el numero de marco de pagina. Luego se tiene de el bit P de presente. Si este es 1 se puede utilizar, pero si es 0 la pagina virtual a la que pertenece la entrada no se encuentra actualmente en la memoria. Los bits de proteccion indican que tipo de acceso esta permitido.  
Los bits de modificada y referenciada llevan el registro de uso de las paginas. Finalmente, el ultimo bit permite deshabilitar el uso de cache para la pagina.  
Cabe destacar que la tabla de paginas solo guarda la informacion que el hardware necesita para traducir una direccion virtual en una direccion fisica.


**Tabla de paginas invertida**: una desventaja del tipo de tablas que se vio es que su tamaño es proporcional al espacio de direcciones virtuales. Una alternativa a esto es la tabla de paginas invertidas. En esta, la parte correspondiente al numero de pagina de la direccion virtual se referencia por medio de un valor hash usando una funcion de hash sencilla. El valor hash es un puntero para la tabla de paginas invertida. Hay una entrada en esta tabla por cada marco de pagina real en lugar de uno por cada pagina vritual. Asi, lo unico que se requiere para estas tablas de pagina siempre es una proporcion fija de la memoria real, independientemente del numero de procesos o de las paginas virtuales soportadas. La estructura de la tabla de paginas de denomina invertida debido a que se indexan sus entradas de la tabla de paginas por el numero de marco en lugar de por el numero de pagina virtual.

#### Tamaño de pagina
Una decision de diseño de hardware importante es el tamaño de pagina.

Cuanto menor sea el tamañano de la pagina, habra menor fragmentacion interna, mas paginas requeridas por proceso lo que lleva a tablas de paginas mas grande, y mas paginas pueden residir en memoria.

Cuanto mayor sea el tamaño de la pagina, mayor sera la fragmentacion interna. Dado que la memoria secundaria esta diseñada para transferir grandes bloques de datos mas eficientemente, se tendra que es mas rapido mover paginas hacia la memoria principal.

#### Buffer de traduccion anticipada
En principio, toda referencia a la memoria virtual puede causar dos accesos a memoria fisica: uno para buscar la entrada de la tabla de paginas apropiada y otro para buscar los datos solicitados, lo que duble el tiempo de acceso a la memoria. PAra resolver este problema, se utiliza una cache especial de alta velocidad para las entradas de la tabla de pagina, habitualmente denominada buffer de traduccion anticipada. Esta cache contiene aquellas entradas de la tabla de paginas que han sido usadas de forma mas reciente.

Dada una direccion virtual, el procesador examina la TLB, si la entrada de la tabla de paginas se encuentra en la TLB, se obtiene el marco y se arma la direccion fisica. Si la entrada no se encuentra ahi, el numero de pagina es usado como indice en la tabla de paginas del proceso. Luego se controla si la pagina esta en la memoria. Finalmente la TLB es actualizada para incluir la nueva entrada.

#### Reemplazo de paginas
Cuando ocurre un fallo de pagina, el sistema operativo tiene que elegir una pagina para desalojarla y hacer espacio para la pagina entrante. Si la pagina a eliminar se modifico mientras estaba en memoria, debe volver a escribirse en el disco para actualizar la copia del mismo. Pero si no se ha modificado, la copia ya esta actualizada y no se necesita rescribir.

Aunque seria posible elegir una pagina al azar para desalojarla en cada fallo de pagina, el rendimiento del sistema es mucho mayor si se selecciona una pagina que no sea de uso frecuente, ya que si se elimina una de uso frecuente, es probable que se la tenga traer de vuelta rapidamente.

Los algoritmos de reemplazo son:

**Algoritmo de reemplazo de paginas optimo**: Al momento en que ocurre un fallo de pagina, hay cierto conjunto de pagins en memoria y una de estas se referenciara en la siguiente instruccion, otras tal vez no se referencien por un tiempo. Cada pagina se puede etiquetar con el numero de instrucciones que se ejecutaran antes de que se haga referencia por primera vez a esa pagina. El algoritmo optimo estable que la pagina con la etiqueta mas alta debe eliminarse.
El problema de este algoritmo es que no se puede implementar. Ya que al momento del fallo de pagina, es imposible que el SO sepa cuando sera la proxima referencia a cada una de las paginas

**No usadas recientemente(NRU)**: Para permitir que el SO recolecte estadisticas utiles sobre el uso de las pagines, se asocian dos bits de estados a cada pagina. El bit **R** se establece cada vez que se hace una referencia, el bit **M** se establece cuando se escribe en la pagina.  
Los bits M y R se pueden utilizar para construir un algoritmo simple de paginacion: cuando se inicia un proceso, ambos bits de pagina para todas las paginas se establecen en 0. El R se borra en forma periodica para diferenciar a las paginas a las que no se ha hecho referencia recientemente de las que se han referenciado.  
Cuando ocurre un fallo de pagina, el sistema operativo inspecciona todas las paginas y las divide en 4 categorias en base a los valores de M y R:
* Clase 0: no ha sido referenciada y no ha sido modificada
* Clase 1: no ha sido referenciada y ha sido modificada
* Clase 2: ha sido referenciada y no ha sido modificada
* Clase 3: ha sido referenciada y ha sido modificada

El algoritmo NRU elimina una pagina al azar de la clase de menor numeracion que no este vacia.

**FIFO**: el sistema operativo mantiene una lista de todas las paginas actualmente en memoria, en donde la llegada mas reciente esta en la parte final y la menos reciente en la parte frontal. En un fallo de pagina, se elimina la pagina que esta en la parte frontal y la nueva pagina se agrega a la parte final de la lista.

**Segunda oportunidad**: una modificacion simple al FIFO que evita el problema de descartar una pagina de uso frecuente es inspeccionar el bit R de la pagina mas antigua. Si es 0, la pagina es antigua y no se ha utilizado, por lo que se sustituye de inmediato. Si el bit R es 1, el bit se borra, la pagina se pone al final de la lista de paginas y su tiempo de carga se actualiza, como si acabara de llegar a la memoria. Despues la busqueda continua.  
Esta operacion se denomina **segunda oportunidad**.  
Este esta buscando una pagina antigua a la que no se haya hecho referencia en el intervalo de reloj mas reciente. Si se ha hecho referencia a todas las paginas, este algoritmo se degenera y se convierte en FIFO puro. La desventaja es innecesariamente ineficiente ya que esta moviendo constatemente paginas en su lista.

**Reloj**: Un mejor metodo seria mantener todos los marcos de pagina en una lista circular en forma de reloj:
![reemplazo_reloj](https://gitlab.com/matipan/final-cso/raw/master/reemplazo_reloj.png)

**Menos usadas recientemente(LRU)**: cuando ocurra un fallo de pagina, hay que descartar la pagina que no se haya utilizado durante la mayor longitud de tiempo.  
Para implementar este, es necesario mantener una lista enlazada de todas las paginas en memoria, con la pagina de uso mas reciente en la parte frontal y la de uso menos reciente en la parte final. La dificultad es que la lista debe actualizarse en cada referencia a memoria. Buscar una pagina en la lista, eliminarla y despues pasarla al frente es una operacion que consume mucho tiempo, incluso mediante hardware(suponiendo que pudiera construirse dicho hardware).

## Trashing
Un sistema esta en trashing cuando pasa mas tiempo paginando que ejecutando procesos, como consecuencia a eso, hay una baja importante de la performance en el sistema.  
El ciclo del trashing es el siguiente:
  1. El SO monitorea el uso del CPU
  2. Si hay baja utilizacion, aumenta el grado de multiprogramacion
  3. Si el algoritmo de reemplazo es global, pueden sacarse marcos a otros procesos.
  4. Un proceso necesita mas marcos, por lo que comienzan los page-faults y robo de marcos a otros procesos.
  5. Por swapping de paginas, y encolamiento en dispositivos, baja el uso de la CPU.
  6. Vuelve a 1

![trashing](https://gitlab.com/matipan/final-cso/raw/master/trashing.png)

Desde el punto de vista del scheduler:
  1. Cuando se decrementa el uso de la CPU, el scheduler long term aumenta el grado de multiprogramacion
  2. El nuevo proceso inicia nuevos page-faults, y por lo tanto, mas actividad de paginado.
  3. Se decremente el uso de la CPU
  4. Vuelve a 1

Para controlar el trashing, se puede limitarlo usando algoritmos de reemplazo local, con este algoritmo, si un proceso entra en trashing no roba marcos a otros procesos. Lo cual perjudica la performance del sistema, pero es controlable.  
Si un proceso contara con todos los frames que necesita, no habria trashing. Para esto se pueden utilizar tecnicas como la del principio de localidad.

### Modelo de localidad
El principio de localidad predice con bastante precision que instrucciones y datos utilizara en un futuro proximo, basandose en las instrucciones y datos que referencio en el pasado.  
La localidad de un proceso en un momento dado se da por el conjunto de paginas que tienen en memoria en ese momento.  
En este modelo, un programa se compone de varias localidades, por ejemplo: cada rutina puede ser una nueva localidad. Los casos mas importantes de localidad son: temporal, espacial y secuencial.
* **Localidad temporal**: si en un momento una posicion de memoria particular es referenciada, entonces es muy probable que la misma ubicacion sea referenciada en un futuro cercano.
* **Localidad espacial**: si una localizacion de memoria es referenciada en un momento concreto, es probable que las localizaciones cercanas a ella tambien sean referenciadas pronto.
* **Localidad secuencial**: las direcciones de memoria que se estan utilizando suelen ser contiguas ya que las instrucciones se ejecutan secuencialmente.

Al momento de realizar un reemplazo de una pagina, se debe considerar el modelo de localidad. Este puede ser local o global.  
Los algoritmos que utilizan un modelo de localidad **local** consideran unicamente a aquellas paginas que estan actualmente asignadas al proceso que genero el fallo de pagina. Cabe destacar que si el conjunto de trabajo crece, se producira sobrepaginacion aun cuando hayan muchos marcos de paginas libres. Si este se reduce, los algoritmos locales desperdician memoria.  
Los algoritmos que utilizan un modelo de localidad **global**, al momento de eliminar la pagina con el menor valor de edad, consideran todas las paginas que estan actualmente en memoria, no solo las que estan asignadas a ese proceso. Estos algoritmos suelen funcionar mejor. El sistema debe decidir continuamente cuantos marcos de pagina asignar a cada proceso.

### Demonio de paginacion
Este es un proceso creado por el SO durante el arranque que apoya a la administracion de la memoria. Se ejecuta cuando el sistema tiene una baja utilizacion o algun parametro de la memoria lo indica: poca memoria libre o mucha memoria modificada.  
Las tareas de este son:
* Limpia las paginas modificadas sincronizandolas con el swap
* Reduce el tiempo de swap posteriormente ya que las paginas estan limpias
* Puede sincronizar varias paginas contiguas reduciendo el tiempo total de transferencia
* Mantener el numero de paginas libres en el sistema a un cierto numero
* No liberarlas del todo hasta que haga falta

## Memoria compartida
Gracias al uso de la tabla de paginas, varios procesos pueden compartir un marco de memoria, para ello este marco debe estar asociado a una pagina en la tabla de paginas de cada proceso. El numero de paginas asociado al marco puede ser diferente en cada proceso.  
![memoria_compartida](https://gitlab.com/matipan/final-cso/raw/master/memoria_compartida.png)

### Copia en escritura - COW
La copia en escritura permite a los procesos padre e hijo compartir inicialmente las mismas paginas de memoria, *si uno de ellos modifica una pagina compartida, la pagina es copiada*.  
COW permite crear procesos de forma mas eficiente debido a que solo las paginas modificadas son duplicadas.

### Mapeo de archivo en memoria
Esta tecnica permite a un proceso asociar el contenido de un archivo a una region de su espacio de direcciones virtuales. El contenido del archivo no se sube a memoria hasta que se generan Page Faults. El contenido de la pagina que genera el PF es obtenido desde el archivo asociado.  
Cuando el proceso termina o el archivo se libera, las paginas modificadas son escritas en el archivo correspondiente. Esto permite realizar E/S de una manera alternativa a usar operaciones directamente sobre el sistema de archivos.

# I/O
Los dispositivos externos dedicados a E/S se pueden agrupar en tres categorias:
* **Legibles par el usuario**: adecuados para la comunicacion con el usuario del computador
* **Legibles para la maquina**: adecuados para la comnucacion con equipamiento electronico.
* **Comunicacion**: adecuados para la comnucacion con dispositivos remotos.

Hay distintas tecnicas para llevar a cabo la E/S. Estas son:
* **E/S programada**: el proceso envia un mandato de E/S, a peticion de un proceso, a un modulo de E/S; a continuacion, ese proceso realiza una espera activa hasta que se complete la operacion antes de continuar.
* **E/S dirigida por interrupciones**: el proceso emite un mandato de E/S a peticion de un proceso y continua ejecutando las instrucciones siguientes, siendo interrumpido por el modulo de E/S cuando este ha completado su trabajo. Las siguientes instrucciones puede ser del mismo proceso, en el caso de que ese proceso no necesite esperar hasta que se complete la E/S. En caso contrario, se suspende el proceso en pesera de la interrupcion y se realiza otro trabajo.
* **Acceso directo de memoria(DMA)**: un modulo de DMA controla el intercambio de datos entre la memoria princiapl y un modulo de E/S. El procesador manda una peticion de transferencia de un bloque de datos al modulo de DMA y resulta interrumpido solo cuando se haya transferido el bloque completo.

### Interfaz I/O
Hay dos objetivos muy importantes en el diseño del sistema de E/S: **eficiencia** y **generalidad**.
* **eficiencia**: es importante debido a que las operaciones de E/S suelen tardar mucho, ya que la mayoria de los dispositivos de E/S son extremadamente lentos comparados con la memoria princiapal y el procesador.
* **generalidad**: es deseable manejar todos los dispositivos de una manera uniforme. esto es tanto para el modoen que los procesos ven los dispositivos de E/S, como a la manera en la que el sistema operativo gestiona los dispositivos y las operaciones de E/S/

# Sistema de ficheros
El sistema de ficheros proporciona las abstracciones de recursos tipicamente asociadas con el almacenamiento secundario. Este permite a los usuarios crear colecciones de datos que:
* Tienen una **existencia de largo plazo**. Estos se almacenan en disco u otro almacenamiento secundario.
* **Compartibles entre procesos**. Estos tienen nombres y pueden tener permisos de acceso asociados que permitan controlar la comparticion
* **Estructura**: dependiendo del sistema de ficheros, un archivo puede tener una estructura interna que es conveniente para aplicaciones particulares.

Los sistemas de ficheros no proporcionan solo una manera de almacenar los datos organizados como ficheros, sino que dan una coleccion de funciones para manipular los ficheros. Algunas son:
* **Crear**:
* **Borrar**
* **Abrir**, un archivo se declara abierto por un proceso para que este pueda realizar acciones sobre el archivo
* **Cerrar**
* **Leer**
* **Escribir**

## Estructura de un fichero
Un fichero suele contener:
* **Campo**, este es el elemento basico de los datos. Un campo individual contiene un unico valor. Dependiendo del diseño del sistema de ficheros, un campo puede tener una longitud fija o variable.
* **Registro**: es una coleccion de campos relaciones que pueden tratarse como una unidad por alguna aplicacion. Estos tambien pueden tener una longitud fija o variable.
* **Fichero**: es una coleccion de campos similares. El fichero se trata como una unitidad unica por parte de los usuarios y las aplicaciones.
* **Base de datos**: es una coleccion de datos relacionados. Esta podria contener toda la informacion relacionada con una organizacion o proyecto. Esta formada por uno o mas tipos de ficheros.

## Sistema de gestion de ficheros
Un sistema de gestion de ficheros es un conjunto de software de sistema que proporciona servicios a los usuarios y aplicaciones en el uso de ficheros. Por lo general la unica forma de acceder a los ficheros, es mediante este sistema.  
Un sistema de gestion debe ser capaz de:
* Satisfacer las necesidades de gstion de datos y requisitos del usuario. Almacenamiento de datos y operaciones
* Garantizar que los datos del fichero son validos
* Optimizar el rendimiento
* Proporcionar soporte de E/S a varios tipos de dispositivos de almacenamiento
* Minimizar o eliminar la potencial perdida de datos
* Proporcionar un conjunto estandar de rutinas de interfaces de E/S a los procesos.
* Proporcionar soporte de E/S a multiples usuarios

### Arquitectura
La siguiente imagen ilustra la arquitectura tipica de un sistema de ficheros:  
![arquitectura_sistema_ficheros](https://gitlab.com/matipan/final-cso/raw/master/arquitectura_sistema_ficheros.png)

En el nivel mas bajo, los **manejadores de dispositivos** se comunican directamente con los dispositivos perifericos o sus controladores o canales. Se suelen considerar parte del sistema operativo.  
En el siguiente nivel se encuentra el **sistema de ficheros basico**. Esta es la interfaz primaria con el entorno fuera del sistema de computacion. Este trata con bloques de datos que son intercambiados con discos o sistemas de cintas. Suele ser parte de sistema operativo.  
Luego esta el **supervisor basico de E/S**, el cual se encarga de todas las inciaciones y finalizaciones de E/S. Las estructuras de cnotrol tratan con los dispositivos de E/S, la planificacion y el estado de los ficheros. Este es parte del sistema operativo.  
La **E/S logica** permite a los usuarios y a las aplicaciones acceder a los registros. Este proporciona una capacidad de E/S de propostio general, a nivel de registros y mantiene datos basicos sobre los ficheros.  
Finalmente, el nivel del fichero mas cercano al usuario suele ser denominado **metodo de acceso**. Proporciona una interfaz estandar entre las aplicaciones y los sistemas de ficheros y dispositivos que contienen los datos. Diferentes metodos de acceso reflejan diferentes estructuras de ficheros y diferentes formas de acceder y procesar los datos.

### Directorios
El directorio contiene informacion sobre los ficheros, incluyendo atributos, ubicacion y propiedad. Gran parte de esta informacion, la gestiona el sistema operativo. El directorio es a su vez un fichero, accesible por varias rutinas de gestion de ficheros.

Para comprender los requisitos de la estructura de un fichero, consideramos los tipos de operaciones que se pueden llevar a cabo sobre directorios:
* **Buscar**: cuando un usuario o aplicacion referencia un fichero, el directorio debe permitir encontrar la entrada correspondiente al fichero.
* **Crear fichero**: cuando se crea un fichero, se debe añadir una entrada al directorio
* **Borrar fichero**: eliminar la entrada del directorio
* **Listar directorio**: se puede solicitar ver el directorio completo o una porcion del mismo.
* **Actualizar directorio**: dado que algunos atributos se almacenan en el directorio, un cambio en uno de ellos requiere un cambio en la entrada de directorio correspondiente.

Una lista sencilla de ficheros no es adecuada para dar soporte a esas operaciones. Ya que los usuarios podrian tener muchos ficheros, querer organizarlos por proyecto o tipo. Si el directorio es una lista secuencial simple, no proporciona ayuda para organizar los ficheros y fuerza a los usuarios a tener cuidado no utilizar el mismo nombre para dos ficheros diferentes.

Una posible solucion a eso seria utilizar un **esquema de dos niveles**. En donde hay un directorio para cada usuario y un directorio maestro. El directorio maestro tiene una entrada por cada directorio de usuario, proporcionando informacion sobre direccion y control de acceso. Cada directorio de usuario es una lista simple de los ficheros de ese usuario, de esta manera los nombres deben ser unicos solamente dentro del directorio de ese usuario, y ademas el sistema de ficheros puede facilmente asegurar las restricciones de acceso de los directorios.

Una forma mas potente y flexible, que esta casi universalmente utilizada, es usar una **estructura jerarquica en forma de arbol**. Donde hay un directorio maestro, que tiene varios directorios de usuario. Cada uno de estos, podria tener subdirectorios y ficheros como entradas.

#### Nombrado
Los ficheros pueden ubicarse siguiente un *path* desde el directorio raiz y sus sucesivas referencias(el path absoluto). De esta manera, distintos archivos pueden tener el mismo nombre, pero el path absoluto es unico. Para no tener que utilizar el path absoluto cada vez que se quiere referenciar un archivo, se nombra al directorio actual como el *directorio de trabajo*. Dentro este, se pueden referenciar los archivos tanto por su path absoluto como por su path relativo, indicando solamente la ruta al archivo desde el directorio de trabajo.

#### Derechos de acceso
El sistema de ficheros debe proporcionar una herramienta flexible para permitir la comparticion de ficheros entre los usuarios. Los derechos de acceso que se pueden asignar a un usuario particular para un determinado fichero son:
* **Ninguno**: ni siquiera puede conocer la existencia del fichero.
* **Conocimiento**: puede determinar que el fichero existe y quien es su propietario.
* **Ejecucion**: puede cargar y ejecutar un programa pero no copiarlo
* **Lecutra**: puede leer el fichero para cualquier propositov, incluyendo copia y ejecucion.
* **Adicion**: puede añadir datos al fichero, por lo general solo al final, pero no puede modificar o borrar los contenidos
* **Actualizacion**: puede modificar, borrar o añadir datos al fichero.
* **Cambio de proteccion**: puede cambiar los derechos de acceso otorgados a otros usuarios
* **Borrado**: puede borrar el fichero del sistema de ficheros.

Se duele determinar como propietario del fichero, a aquel usuario que lo creo. Este tiene todos los derechos de acceso que se listaron y puede conceder permisos a otros usuarios. Se pueden proporcionar diferentes acceso a diferentes clases de usuarios:
* **Usuario especifico**: usuarios individuales que se designan por el identificado del usuario.
* **Grupos de usuarios**: acceso a todos los usuarios que formen parte de cierto grupo
* **Todos**: todos los usuarios con acceso a este sistema.

## Gestion de almacenamiento secundario
En almacenamiento secundario, un fichero esta compuesto por una coleccion de bloques. El SO es responsable de asignar bloques a los ficheros. Primero se debe asignar espacio de almacenamiento secundario a los ficheros, y segundo es necesario guardar una traza del espacio disponible para su asignacion.

### Preasignacion frente a asignacion dinamica
Una politica de preasignacion requiere que el tamaño maximo de un fichero sea declarado en tiempo de creacion de fichero. En varios casos se puede estimar el tamaño de forma fiable. Pero en muchos otros, es dificil y hasta imposible estimar el tamaño maximo potencial del fichero. En estos casos, los usuarios y programadores tendenria a sobreestimar el tamaño de fichero para que no se quede sin espacio. Por lo tanto, hay ventajas en el uso de la gestion dinamica, que asigna espacio a un fichero en porciones cuando se necesite.  

### Metodos de asignacion de ficheros
Hay tres metodos de uso comun: **contiguo**, **encadenado** e **indexado**. Las caractiristicas de cada metodo se pueden ver en esta tabla:  
![tabla_metodos_asignacion](https://gitlab.com/matipan/final-cso/raw/master/tabla_metodos_asignacion.png)

* **Asignacion contigua**: se asigna un unico conjunto de blqoeus en tiempo de creacion de los ficheros. Por ende, hay una estrategia de preasignacion que utiliza porciones de tamaño variable. Esta es la mejor desde el punto de vista del fichero secuencial individual. Multiples bloques se pueden leer de una vez para mejorar el rendimiento de E/S en procesamiento secuencial. En esta existe *fragmentacion interna*, haciendo dificil encontrar bloques contiguos de espacio de suficiente longitud. De vez en cuando sera necesario llevar acabo un algoritmo de compactacion para liberar espacio adicional en el disco.
![asignacion_contigua](https://gitlab.com/matipan/final-cso/raw/master/asignacion_contigua.png)
* **Asignacion encadenada**: la asignacion se realiza a nivel de bloques individuales. Cada bloque contiene un puntero al siguiente bloque en la cadena. La seleccion de bloques es una cuestion sencilla: cualquier bloque libre se puede añadir a una cadena. No hay fragmentacion externa porque solo se necesita un bloque cada vez. Una consecuencia es que no existe principio de proximidad. Por tanto, si es necesario traer varios bloques de fichero a la vez, se requiere una seria de acceso a diferentes partes del disco.
![asignacion_encadenada](https://gitlab.com/matipan/final-cso/raw/master/asignacion_encadenada.png)
* **Asignacion indexada**: resuelves muchos de los problemas de la asignacion contigua y encadenada. La tabla de asignacoin de ficheros contiene un indice separada de un nivel por cada fichero, el indice tiene una entrada por cada porcion asignada al fichero. Estos no se almacenan fisicamente como parte de la tabla de asignacion de ficheros. Se guardan en un bloque separado y la entrada para ficheros en la tabla de asignacion de ficheros apunta dicho bloque.  
![asignacion_indexada](https://gitlab.com/matipan/final-cso/raw/master/asignacion_indexada.png)

### Gestion de espacio libre
De la misma forma que se asigna el espacio a los ficheros, se debe gestioanr el espacio que no esta actualmente asignado a ningun fichero. Para llevar a cabo la asignacion de ficheros, es necesario saber que bloques estan disponibles en el disco. Por tanto, se necesita un **tabla de asignacion de disco** ademas de la tabla de asignacion de ficheros.

#### Tabla de bits
Utiliza un vector que esta formado por un bit por cada bloque en el disco. Cada entrada 0 corresponde a un bloque libre y cada 1 corresponde a un bloque en uso. La ventaja de este es que es facil encontrar un bloque libre o un grupo contiguo de bloques libres. Una tabla de bits trabaja bien con cualquiera de los metodos de asignacion de ficheros descrito. La desventaja es que incluso teniendo la tabla de bits en memoria principal, una busqueda exhaustiva puede ralentizar el rendimiento del sistema de ficheros.

#### Porciones libres encadenadas
Las porciones libres se pueden encadenar utilizando un puntero y valor de longitud de cada porcion libre. Este metodo tiene una sobrecarga de espacio insignificante, porque no se necesita una tabla de asignacion de disco, sino simplemente un puntero al comienzo de la cadena y la longitud de la primera porcion. Este metodo sirve para todos los metodos de asignacion de ficheros. Pero, despues de cierto uso, el disco se quedara fragmentado y muchas porciones seran de la longitud de un unico bloque. Ademas, cada vez que se asigne un bloque, se necesita leer el bloque primero para recuperar el puntero al nuevo primer bloque libre antes de escribir los datos en dicho bloque.

#### Indexacion
Esta trata al espacio libre como un fichero y utiliza una tabla de indices tal y como se describio en la asignacion de ficheros. Por razones de eficiencia, el indice se deberia utilizar en base a porciones de tamaño variable en lugar de bloques. Esta tecnica proporciona soporte eficiente a todos los metodos de asignacion.

## Gestion de ficheros de Unix
En el sistema de ficheros de Unix, hay sies tipos de ficheros:
* **Regulares u ordinarios**: contiene datos arbitrarios en cero o mas bloques de datos. Contienen informacion introducida por un usuario, una aplicacion o una utilidad del sistema.
* **Directorios**: contiene una lista de nombres de ficheros msa punteros a nodos-i asociados. Son realmente ficheros ordinarios con privilegios de proteccion de escritura especiales de tal forma que solo el sistema de ficheros puede escribirlos, mientras que los programas de usuario tienen acceso de lectura.
* **Especiales**: no contienen datos, sino que proporcionan un mecanismo para asociar dispositivos fisicos a nombres de ficheros. Cada dispotivo de E/S se asocia con un fichero especial.
* **Tuberias con nombre**: una tuberia es una utilidad de comunicacion entre procesos. Una tuberia guarda en un buffer los datos de su entrada de forma que un proceso que lea de la salida de la tuberia reciba los datos del mismo modo que si leyera de una cola FIFO.
* **Enlaces**: un enlace es un nombre alternativo de fichero para un fichero existente.
* **Enlaces simbolicos**: un fichero de datos que contiene el nombre del fichero al que enlaza.

### NODOS-I
Todos los tios de ficheros de Unix se administran por el sistema operativo mediante los nodos-i. Un nodo indice es una estructura de control que contiene informacion clave necesaria de un fichero particular para el sistema operativo. Varios nombres de ficheros se pueden asociar con un unico nodo-i, pero un nodo-i activo se asocia con exactamente un fichero, y cada fichero es controlado por exactamente un nodo-i.  
Los atributos del fichero asi como sus permisos y otra informacion de control se almacenan en el nodo-i.  
En el disco, hay una tabla o lista de nodos-i que contiene todos los nodos-i de todos los ficheros del sistema de ficheros. Cuando se abre un fichero se trae su nodo-i a memoria principal y se almacena en una tabla de nodos-i residente en memoria.

### Estructura del volumen
Un sistema de ficheros UNIX reside en un unico disco logico o particion de disco, y se compone de los siguientes elementos:
* **Bloque de arranque**: contiene el codigo requerido para arrancar el sistema operativo.
* **Superbloque**: Contiene atributos e informacion sobre el sistema de ficheros, tal como el tamaño de la particion y el tamaño de la tabla de nodos-i.
* **Tabla de nodos-i**: la coleccion de nodos-i para cada fichero.
* **Bloque de datos**: el espacio de almacenamiento disponible para los ficheros de datos y subdirectorios.

# Buffer cache
Para incrementar la eficiencia del sistema, el kernel contiene una conjunto de buffers, denominado buffer cache, el cual contiene datos que se usaron recientemente en bloques del disco. Estos buffers se almacenan en memoria principal, con el objetivo de minimizar la frecuencia de acceso al disco.  
Cuando un proceso quiere acceder a un bloque de la cache hay dos alternativas:
* Se copia el espacio de direcciones de usuario
* Se trabaja como memoria compartida(no se copia, permitiendo acceso a varios procesos)

### Estrategia de reemplazo
Cuando se necesita un buffer para cargar un nuevo bloque, se elige el que hace mas tiempo no es referenciado. Para esto se usa una lista de bloques, donde el ultimo es el menos recientemente usado. Entonces, cuando el bloque se referencia o entra en la cache, este queda al final de la lista.  
Dentro de la memoria, los bloques no se mueven, sino que se asocian punteros.

## Buffer Cache Unix System V
La caché de buffer en UNIX es una caché de disco. Como la caché de buffer y la zona de E/S del proceso residen ambas en la memoria principal, se usará DMA para llevar a cabo una copia de memoria a memoria. Esta acción no gastará ningún ciclo del procesador, pero consumirá ciclos del bus.  
Cuando se hace una referencia a un número de bloque físico de un dispositivo en particular, el sistema operativo comprueba primero si el bloque está en el buffer de la caché. Para minimizar el tiempo de búsqueda, la lista de dispositivos se organiza como una tabla de dispersión.  
Después de que se haya asignado un buffer a un bloque de disco, no podrá ser usado por otro bloque hasta que todos los demás buffers se hayan usado.  

Un buffer esta compuesto de dos partes: un arreglo que contiene la informacion que se trajo del disco y la cabecera que identifica el buffer. Los datos dentro del buffer corresponden a un bloque del disco. Este bloque nunca puede estar mapeado a mas de un buffer. Ya que si dos buffers tienen los datos de un solo bloque, el kernel no sabria cual de los contiene la informacion actual.  
La **cabecera del buffer** contiene varios campos. Un campo que indica el **numero de dispositivo**, otro que indica el **numero de bloque**. Un puntero al arreglo de los datos del buffer. Y un campo que indica el estado del buffer, el cual puede ser:
* **Bloqueado**
* **Libre o disponible**
* El kernel esta escribiendo a disco o leyendo del disco
* **Escritura retrasada**: buffers que hayan sido modificados en memoria pero el bloque original en disco todavia no fue actualizado

![cabecera_unix](https://gitlab.com/matipan/final-cso/raw/master/cabecera_unix.png)

### Estructura
El kernel realiza los cacheos de la informacion utilizando el algoritmo LRU(least frequently used): luego que asigna un buffer a un bloque de disco, no puede usar el buffer para otro bloque hasta que el resto de los buffers hayan sido usados recientemente.  
El kernel mantiene una **free list(lista libre)** que mantiene el orden de los menos recientemente usados. Cuando el sistema inicia, se pone a todos los buffers en la lista libre. Cuando el kernel quiere cualquier buffer libre, toma aquel que esta en la cabeza de la lista y lo elimina de la lista. De esta manera, los buffers que estan mas cerca a la cabeza son aquellos que han sido usados menos recientemente.  
![free_list_unix](https://gitlab.com/matipan/final-cso/raw/master/free_list_unix.png)

Cuando el kernel accede a un bloque de disco, busca el buffer con la combinacion dispositivo-bloque correspondiente. En vez de buscar todos los buffers, mantiene colas denominadas **hashed queues**, donde se organizan los buffers segun una funcion de hasheo usando numero de dispositivo y de bloque.

Hay distintos escenarios para la busqueda y recuperacion de un buffer:  
**1er escenario**: El kernel encuentra el bloque en la hash queueu, el kernel esta disponible por end esta en la free list. Se remueve ese buffer de la free list, pasa al estado Ocupado y luego el proceso puede usar el bloque.

**2do escenario**: El bloque buscado no esta en la hash queue. Entonces, se toma un buffer de la free list(siempre se usa el primero). Se lee del disco el bloque deseado en el buffer obtenido y se ubica en la hash queue correspondiente.

**3er escenario**: El kernel no encuentra el bloque buscado en la hash queue. Procede a tomar el primero de la free list, pero este esta en estado de **escritura retrasada**(fue modificado en memoria pero el bloque del disco no fue actualizado). Entonces, el kernel debe mandar ese bloque a disco y tomar otro buffer de la free list, si tambien esta en **escritura retrasada** continua hasta encontrar uno que no lo este. Mientras manda a escribir a disco los que estan en escritura retrasada, asigna el siguiente libre. Una vez escritos a disco los bloques de escritura retrasada, se los ubica al principio de la free list.

**4to escenario**: el kernel no encuentra el bloque en la hash queue y la free list esta vacia. Entonces, el proceso debe esperar a que se libere algun buffer. Cuando el proceso despierta, debe volver a verificar que el bloque no este en la hash queue ya que algun proceso pudo haberlo cargado mientras este dormia.

**5to escenario**: el kernel busca un bloque y el buffer que lo contiene esta marcado como **ocupado**, para esto el proceso queda esperando a que este libre.
