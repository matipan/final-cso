# System Calls
* Explicar como pueden ser implementadas, tener en cuenta: participacion o apoyo del hardware, modos de ejecucion y uso de pilas
* Un proceso x ejecuta una system call bloqueante de proceso como read, pasos que suceden hasta ejecutar un nuevo proceso y estructuras y modulos de planificacion
* Suponiendo que un proceso ejecuta la System call "fork()", describa eventos y actividades que suceden hasta que el nuevo proceso comienza su ejecucion

# Procesos
* Describa estados de un proceso. Planificadores, su relacion con las transiciones entre los estados numerados
* Describa las estructuras de datos y actividades llevadas a cabo para implementar el manejo de procesos. Tener en cuenta: planificación, estados, módulos, etc.
* Considere un proceso X que se encuentra en Ejecucion y que se le termina su tiempo de CPU. De acuerdo a la planificacion utilizada se le debera otorgar la CPU a un proceso Y. Indique el manejo de las colas de procesos, como se relacionan y modifican. Estructuras que participan. Detalle como se realiza el cambio de contexto


# Memoria
* Memoria virtual paginacion por demanda
  Estructuras, apoyo del HW, actividades del SO
  Page faults, que son, quien los detecta, quien los resuelva y como.
  Definicion de Trashing como descubrirlo y tratarlo. Que relacion tienen con los planificadores de CPU
* Explique la tecnica de administracion de memoria: segmentacion paginada. Tener en cuenta estructuras usadas, tareas desarrolladas por el SO y apoyo que debe brindar el HW para las mismas.

# Sistema de archivos
* Describir la estructura del file system de Unix System V
* Pasos en la creacion de un archivo, como acceder por ejemplo a home/usr/final.txt. Modificacion de estructuras y estructuras creadas con la creacion del archivo

# Sistemas de archivos, sistemas de E/S y buffer cache. Explique y relacione los 3 componentes y su interaccion dentro del SO
