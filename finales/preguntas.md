#### System calls:
* Definición. Explicar como pueden ser implementadas, para ello tenga en cuenta: participación o apoyo del harware, modos de ejecución, stacks (pilas).
* Suponiendo que un proceso ejecuta la System Call "fork()", describa eventos y actividades que se suceden hasta que el nuevo proceso comienza su ejecución (tener en cuenta planificadores, dispatcher, etc)

#### System Calls
* Definición, como se implementan, participación o apoyo del hardware, modos de ejecución, uso de pilas.
* Un proceso x ejecuta una System call bloqueante de proceso como read, pasos que se suceden hasta ejecutar un nuevo proceso y (estructuras y módulos de planificación)

#### System calls
* Porque son necesarias? Porque no pueden ejecutarse en modo usuario?
* Se quiere ejecutar la instruccion que implica una lectura de una fila de una base de datos que esta en disco. Considerando que el controlador no sabe de bases de datos... como se transforma esa instruccion en una orden para el controlador de disco? Cual seria ese comando? Load o store?

#### System calls. Defincion, explicar su implementacion, para ello tenga en cuenta participacion o apoyo del hardware. Modos de ejecucion, stacks(pilas) y todo aquel que involucre su ejecucion.

#### Describa estados de un proceso. Planificadores, su relacion con las transiciones entre los estados numerados.

#### Considere un proceso X que se encuentra en ejecucion y que se le termina su tiempo de CPU. De acuerdo a la planificacion utilizada se le debera otorgar la CPU a un proceso Y.
* Indique el manejo de las colas de procesos, como se relacionan y modifican. Estructuras que participan
* Detalle como se realiza el cambio de contexto

#### Suponiendo que un proceso X ejecuta una System call que ocasiona que el proceso sea bloqueado, por ejemplo write(), analice y describa que eventos, actividades se suceden hasta que otro proceso Y comienza su ejecucion. Tener en cuenta planificadores, dispatchers, etc.

#### Se quiere implementar el concepto de proceso junto a algún algoritmo de planificación de CPU en un SO 
* ¿Cómo lo implementaría? Tenga en cuenta el apoyo del HW, estructuras de datos , tareas que deba realizar el SO

#### Se quiere implementar el concepto de "proceso" junto con el "algoritmo de planificación de CPU round robin" en un SO. Como los implementaria? Tenga en cuenta el apoyo del HW que necesita y con que fin(interrupciones, modos de ejecucion, etc) asi como las estructuras de datos que necesitara el mantenidas por el SO asi como que tareas debera realizar el mismo, implementacion del Quantum, tareas realizadas cada vez que un quantum se consume, etc.

#### Explique la técnica de administración de memoria : segmentación paginada.
* Tenga en cuenta estructuras utilizadas, tareas desarrolladas por el SO y apoyo que debe brindar el HW pasar las mismas

#### Describa como se implementa memoria virtual utilizando Segmentacion por demanda. Tenga en cuenta estructuras utilizadas, participacion del hardware, tareas del sistema operativo. Ventajas y desventajas respecto a Paginacion por demanda.

#### Memoria Virtual paginación por demanda.
* Estructuras, apoyo del HW, actividades del SO.
* Page faults. Trashing definición, cómo descubrirlo y cómo tratarlo.

#### Memoria virtual paginacion por demanda
* Fallos de pagina. Que son? Quien los detecta? Quien los resuelve? Como se resuelven?
* Explique y relacione los siguientes conceptos: Hiperpaginacion(trashing), planificadores de CPU

#### Memoria virtual con paginación por demanda:
* Funcionamiento. Estructuras utilizadas, participación del hardware, actividades desarrolladas por el SO.
* Fallos de páginas: Explicar como se resuelven y que estructuras de las descriptas en el punto anterior son utilizadas (tanto modificadas, creadas y/o consultadas)

#### Sistema de archivos describir la estructura del file system de UNIX System V.
* Pasos en la creación de un archivo, cómo acceder por ejemplo a homeusrfinal.txt.
* Modificación de estructuras, consulta y estructuras creadas con la creación del archivo.

#### Sistemas de archivos. Describa la estructura del sistema de archivos de UNIX system V.
* Si quisiera crear un nuevo archivo, por ejemplo en el directorio /home/usuario/notas.txt, que pasos suceden y que estructuras de las enumeradas son modificadas o creadas?

#### Sistemas de archivos , Sistemas de E/S y buffer cache 
* Explique y relacione los 3 componentes y su interaccion dentro del so.

#### Sistemas de archivos.
* Describa la estructura del sistema de archivos de UNIX System V.
* ¿Si quisiera crear un nuevo archivo, por ejemplo /home/usuario/final.txt, que estructuras de las descriptas en el 

#### Sistemas de archivo. Buffer cache: relacione los 2 subsistemas de un SO y su interaccion. Para un desarrollo ordenado utilice la SysCall "read(fs,buff, count)" indicando las tareas que se suceden desde que un proceso la ejecuta hasta que le es devuelto el control al mismo.

#### Sistema de E/S. Describa el sistema de archivos en Unix System V. describa los pasos necesarios para abrir un archivo open('/home/iso/finales.doc').
